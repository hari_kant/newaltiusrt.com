<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SAAS Starups</title>

        <!-- plugins -->
        <link href="assets/css/plugins/bundle.css" rel="stylesheet">
        <!--main css file-->
        <link href="assets/css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="boxed-layout">
          <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->
        <!--back to top-->
        <a href="#" class="scrollToTop"><i class="ion-ios-arrow-up"></i></a>
        <!--back to top end-->
        <section class="wrapper-boxed">

			@yield('content')


        </section>
        <!-- jQuery plugins-->
        <script src="assets/js/plugins/plugins.js"></script> 
        <script src="assets/js/template-custom.js" type="text/javascript"></script>
			@yield('scripts')
    </body>
</html>
