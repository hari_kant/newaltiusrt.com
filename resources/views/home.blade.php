@extends('layouts.boxed')


@section('content')

			@include('theme.section-nav')
			@include('theme.section-dzsparallaxer')
			@include('theme.section-features')

			@include('theme.section-showcase')
			@include('theme.section-pricing')
			@include('theme.section-testimonials')
			@include('theme.section-steps')
			@include('theme.section-features2')

			@include('theme.section-app')
			@include('theme.section-partners')
			<div class="space-70"></div>
			@include('theme.section-trial')
			@include('theme.footer')


@endsection
