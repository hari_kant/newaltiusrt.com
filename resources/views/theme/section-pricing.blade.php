        <section class="pricing-section">
            <div class="space-90"></div>
            <div class="container">
                <div class="center-title">
                    <h2>Easy plans. Cancel anytime.</h2>
                    <p>All Plans Include a 14 day free trail</p>
                </div>
                <div class="row">
                    <div class="col-lg-4 margin-b-30">
                        <div class="price-box">
                            <div class="price-header">
                                <h1>Free</h1>
                                <h4>Basic Plan</h4>
                            </div>
                            <ul class="list-unstyled price-features">
                                <li>5 Team Members</li>
                                <li>6 Month Support</li>
                                <li>10 Pages</li>
                                <li>1 Domain</li>
                                <li>25 GB Storage</li>
                            </ul>
                            <div class="price-footer">
                                <a href="#" class="btn btn-rounded btn-dark-border">Sign Up</a>
                            </div>
                        </div>
                    </div><!--/col-->
                    <div class="col-lg-4 margin-b-30">
                        <div class="price-box best-plan">
                            <div class="price-header">

                                <h1><span class="dolor">$</span>9.99 <span class="peroid">/Month</span></h1>
                                <h4>Pro Plan</h4>
                            </div>
                            <ul class="list-unstyled price-features">
                                <li>15 Team Members</li>
                                <li>12 Month Support</li>
                                <li>30 Pages</li>
                                <li>5 Domain</li>
                                <li>100 GB Storage</li>
                            </ul>
                            <div class="price-footer">
                                <a href="#" class="btn btn-rounded btn-white-border">Sign Up</a>
                            </div>
                        </div>
                    </div><!--/col-->
                    <div class="col-lg-4 margin-b-30">
                        <div class="price-box">
                            <div class="price-header">

                                <h1><span class="dolor">$</span>29.99 <span class="peroid">/Month</span></h1>
                                <h4>Gold Plan</h4>
                            </div>
                            <ul class="list-unstyled price-features">
                                <li>25 Team Members</li>
                                <li>24/7 Support</li>
                                <li>Unlimited Pages</li>
                                <li>10 Domain</li>
                                <li>Unlimited Storage</li>
                            </ul>
                            <div class="price-footer">
                                <a href="#" class="btn btn-rounded btn-dark-border">Sign Up</a>
                            </div>
                        </div>
                    </div><!--/col-->
                </div>
            </div>
            <div class="space-60"></div>
        </section><!--end pricing section-->
