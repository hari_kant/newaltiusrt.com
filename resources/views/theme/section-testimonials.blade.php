        <section class="testimonials skin-bg">
            <div class="space-90"></div>
            <div class="container">
                <div class="center-title">
                    <h2>True words from our customers.</h2>
                    <p>Over 500,000 Customers worldwide</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 margin-b-30">
                        <div class="feedback-box">
                            <p>
                                " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rhoncus magna a lacinia tempor. Fusce et turpis posuere, imperdiet orci et, tempus ex. Nunc vitae pellentesque ipsum. Quisque id magna a nisl vestibulum ullamcorper. "
                            </p>
                        </div>
                        <div class="testi-info">
                            <img src="assets/images/avtar-1.jpg" alt="" class="rounded-circle" width="60">
                            <div class="content">
                                <h4>Nikita Miller</h4>
                                <em>Saas Customer</em>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 margin-b-30">
                        <div class="feedback-box">
                            <p>
                                " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rhoncus magna a lacinia tempor. Fusce et turpis posuere, imperdiet orci et, tempus ex. Nunc vitae pellentesque ipsum. Quisque id magna a nisl vestibulum ullamcorper. "
                            </p>
                        </div>
                        <div class="testi-info">
                            <img src="assets/images/avtar-2.jpg" alt="" class="rounded-circle" width="60">
                            <div class="content">
                                <h4>John Doe</h4>
                                <em>Saas Customer</em>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 margin-b-30">
                        <div class="feedback-box">
                            <p>
                                " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rhoncus magna a lacinia tempor. Fusce et turpis posuere, imperdiet orci et, tempus ex. Nunc vitae pellentesque ipsum. Quisque id magna a nisl vestibulum ullamcorper. "
                            </p>
                        </div>
                        <div class="testi-info">
                            <img src="assets/images/avtar-3.jpg" alt="" class="rounded-circle" width="60">
                            <div class="content">
                                <h4>Emily Howkins</h4>
                                <em>Saas Customer</em>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-60"></div>
        </section><!--end testimonials section-->
