        <section class="steps">
            <div class="space-90"></div>
            <div class="container">
                <div class="center-title">
                    <h2>Easy to get started. 3 Step process.</h2>
                    <p>Take a look what the people think about our product</p>
                </div>
                <div class="row">
                    <div class="col-md-4 margin-b-30">
                        <div class="step-box">
                            <h1>01</h1>
                            <h4>Create Account.</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque efficitur turpis, vitae dictum dolor tristique in.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 margin-b-30">
                        <div class="step-box">
                            <h1>02</h1>
                            <h4>Select Plan.</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque efficitur turpis, vitae dictum dolor tristique in.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 margin-b-30">
                        <div class="step-box">
                            <h1>03</h1>
                            <h4>Assign Tasks.</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque efficitur turpis, vitae dictum dolor tristique in.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="space-60"></div>
        </section><!--end steps-->