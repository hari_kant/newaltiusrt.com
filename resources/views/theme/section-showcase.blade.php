        <section class="showcase-section">
            <div class="space-90"></div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 margin-b-30">
                        <h3>
                            Easily customizable with great user interface.
                        </h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque efficitur turpis, vitae dictum dolor tristique in. Mauris tristique id urna at cursus. Aliquam maximus, ligula nec commodo maximus, felis metus sagittis ligula, lobortis consequat ante risus ut elit.
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list">
                                    <li><i class="ion-ios-arrow-thin-right"></i>Lorem ipsum dolor</li>
                                    <li><i class="ion-ios-arrow-thin-right"></i>Duis lacinia dolor quis</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="list">
                                    <li><i class="ion-ios-arrow-thin-right"></i>Lorem ipsum dolor</li>
                                    <li><i class="ion-ios-arrow-thin-right"></i>Duis lacinia dolor</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 margin-b-30">
                        <img src="assets/images/shot1.png" alt="" class="img-fluid center-img">
                    </div>
                </div>
                <div class="space-60"></div>
            </div>
        </section><!--end showcase section-->
        <section class="showcase-section bg-faded">
            <div class="space-90"></div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6 ml-auto margin-b-30">
                        <img src="assets/images/iphone.png" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-5 col-md-6 mr-auto margin-b-30">
                        <h3>
                            A fully featured & well designed template that works perfectly on all devices.
                        </h3>
                        <p class="margin-b-20">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque efficitur turpis, vitae dictum dolor tristique in. Mauris tristique id urna at cursus. Aliquam maximus, ligula nec commodo maximus, felis metus sagittis ligula, lobortis consequat ante risus ut elit.
                        </p>
                        <a href="#" class="btn btn-lg btn-primary btn-rounded">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="space-30"></div>
        </section><!--end showcase section-->