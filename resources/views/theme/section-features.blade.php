        <section class="features-section border-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-3 margin-b-30">
                        <div class="feature-box-center text-center">
                            <i class="ion-iphone"></i>
                            <h4 class="text-capitalize">Fully Responsive</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                            </p>
                        </div>
                    </div><!--/col-->
                    <div class="col-md-6 col-lg-3  margin-b-30">
                        <div class="feature-box-center text-center">
                            <i class="ion-ios-person-outline"></i>
                            <h4 class="text-capitalize">User Friendly</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                            </p>
                        </div>
                    </div><!--/col-->
                    <div class="col-md-6 col-lg-3  margin-b-30">
                        <div class="feature-box-center text-center">
                            <i class="ion-ios-cog-outline"></i>
                            <h4 class="text-capitalize">Well Documented</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                            </p>
                        </div>
                    </div><!--/col-->
                    <div class="col-md-6 col-lg-3 margin-b-30">
                        <div class="feature-box-center text-center">
                            <i class="ion-key"></i>
                            <h4 class="text-capitalize">Easy & Customizable</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                            </p>
                        </div>
                    </div><!--/col-->
                </div>
            </div>
        </section><!--end features section-->
