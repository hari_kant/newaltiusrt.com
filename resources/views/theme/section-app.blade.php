        <section class="app-section bg-faded">
            <div class="space-90"></div>
            <div class="container">
                <div class="row align-items-center">

                    <div class="col-lg-6 margin-b-30">
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="854" height="480" src="https://www.youtube.com/embed/8zxj-6smeVA" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-lg-5 ml-auto">
                        <h3>Download our mobile app.</h3>
                        <p class="margin-b-20">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rhoncus magna a lacinia tempor. Fusce et turpis posuere, imperdiet orci et, tempus ex. Nunc vitae pellentesque ipsum. Quisque id magna a nisl vestibulum ullamcorper.
                        </p>
                        <div class="app-buttons">
                            <a href="#"><img src="assets/images/app-store.png" alt=""></a>
                            <a href="#"><img src="assets/images/play-store.png" alt=""></a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="space-60"></div>
        </section>