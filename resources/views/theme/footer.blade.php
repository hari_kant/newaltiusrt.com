        <footer class="footer">
            <div class="space-50"></div>
            <div class="container">
                <div class="row vertical-align-child">
                    <div class="col-md-3 margin-b-30">
                        <div class="margin-b-20">
                            <a href="#">
                                <img src="assets/images/logo-light.png" alt="">
                            </a> 
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque efficitur turpis, vitae dictum dolor tristique in.
                        </p>
                    </div>
                    <div class="col-md-2 margin-b-30">
                        <ul class="list-unstyled">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Terms</a></li>
                            <li><a href="#">Privacy</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 margin-b-30">
                        <ul class="list-unstyled">
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Press Kit</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-5 margin-b-30">
                        <ul class="list-inline social">
                            <li class="list-inline-item"><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-googleplus"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-linkedin"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-rss"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-youtube"></i></a></li>
                        </ul>
                        <h4>Subscribe to newsletter</h4>
                        <form class="signup-form navbar-form margin-b-20">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter your email address">
                            </div>   
                            <button type="submit" class="btn btn-cta btn-primary btn-lg">Subscribe</button>                                 
                        </form>
                        <span>&copy; Copyright 2016 - 2020 </span>
                    </div>
                </div>
            </div>
            <div class="space-20"></div>
        </footer>
