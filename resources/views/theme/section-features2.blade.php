        <section class="features-bg">
            <div class="container">
                <div class="center-title">
                    <h2>Why choose SAAS</h2>
                    <p>Over 500,000 Customers worldwide</p>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="feature-icon-left clearfix">
                            <div class="icon">
                                <i class="ion-iphone"></i>
                            </div>
                            <div class="content">
                                <h4>Fully Responsive</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                                </p>
                            </div>
                        </div><!--/feature icon-->
                        <div class="feature-icon-left clearfix">
                            <div class="icon">
                                <i class="ion-social-google"></i>
                            </div>
                            <div class="content">
                                <h4>Google fonts</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                                </p>
                            </div>
                        </div><!--/feature icon-->
                        <div class="feature-icon-left clearfix">
                            <div class="icon">
                                <i class="ion-ios-copy"></i>
                            </div>
                            <div class="content">
                                <h4>Well documented</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                                </p>
                            </div>
                        </div><!--/feature icon-->
                    </div>
                    <div class="col-md-6">
                        <div class="feature-icon-left clearfix">
                            <div class="icon">
                                <i class="ion-social-twitter"></i>
                            </div>
                            <div class="content">
                                <h4>Bootstrap based</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                                </p>
                            </div>
                        </div><!--/feature icon-->
                        <div class="feature-icon-left clearfix">
                            <div class="icon">
                                <i class="ion-ios-person"></i>
                            </div>
                            <div class="content">
                                <h4>Item Support</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                                </p>
                            </div>
                        </div><!--/feature icon-->
                        <div class="feature-icon-left clearfix">
                            <div class="icon">
                                <i class="ion-ios-cog"></i>
                            </div>
                            <div class="content">
                                <h4>Easy to use</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.
                                </p>
                            </div>
                        </div><!--/feature icon-->
                    </div>
                </div>
            </div>
        </section>

