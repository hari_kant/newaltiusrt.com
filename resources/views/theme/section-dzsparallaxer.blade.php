            <section id="home" class="dzsparallaxer auto-init dzsparallaxer---window-height use-loading" style="position: relative; height: 800px;" data-options='{  mode_scroll: "normal" }'>

                <div class="divimage dzsparallaxer--target " data-src="assets/images/bg1.jpg" style="width: 100%; height: 130%; background-image: url()">
                    <div class="hero-parallax">
                        <div class="hero-inner">
                            <div class="hero-content">
                                <div class="container text-center">
                                    <div class="row">
                                        <div class="col-md-8 ml-auto mr-auto">
                                            <div class="hero-static">
                                                <h3 class="display-4">Perfect choice for your web and mobile apps.</h3>
                                                <p class="lead">
                                                    Create a website that will impress your clients & visitors
                                                </p>
                                                <div class="buttons">
                                                   <a href="https://www.youtube.com/watch?v=8zxj-6smeVA" class="btn btn-xl btn-primary video-popup">Video Tour <i class="ion-play"></i></a>
                                                    <a href="#" class="btn btn-xl btn-white-border">Sign Up Free</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--container-->
                            </div><!--hero content-->
                        </div><!--hero inner-->

                    </div><!--parallax hero-->
                </div><!--parallax image div-->
            </section>
